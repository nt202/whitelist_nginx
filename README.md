### What it does?
After saving new file - exeutes `systemctl reload nginx`

### Service:
```bash
sudo nano /etc/systemd/system/whitelist_nginx.service
```

```service
[Unit]
Description=whitelist_nginx service
After=network.target

[Service]
WorkingDirectory=/Programs/whitelist_nginx
ExecStart=/Programs/whitelist_nginx/whitelist_nginx
User=root
Group=root

[Install]
WantedBy=multi-user.target
```

```bash
sudo systemctl start whitelist_nginx.service
sudo systemctl enable whitelist_nginx.service
sudo reboot
```

### .env:
```
PORT=11435
POSTGRES_HOST=ep-lovely-database.eu-central-1.aws.neon.tech
POSTGRES_PORT=5432
POSTGRES_DB_NAME=neondb
POSTGRES_USER=username
POSTGRES_PASSWORD=Uva2xNkYWjWF
POSTGRES_SSLMODE=require
DESTINATION=/etc/nginx/whitelist.txt
```

### Example:
##### App service:
```nginx
server {
    server_name example.com;
    listen 6666;

    server_tokens off;
    
    location / {
        add_header Access-Control-Allow-Origin "*";
        add_header Access-Control-Allow-Headers "*";
        add_header Access-Control-Allow-Methods "*";
        proxy_set_header Host $host;
        proxy_pass https://127.0.0.1:11435/;
    }
}
```

##### Main service:
```nginx
server {
    server_name example.com;
    listen 7777;

    include /etc/nginx/whitelist.txt;
    deny all;

    server_tokens off;
    client_max_body_size 512M;
    
    location / {
        add_header Access-Control-Allow-Origin "*";
        add_header Access-Control-Allow-Headers "*";
        add_header Access-Control-Allow-Methods "*";
        proxy_set_header Host $host;
        proxy_pass https://127.0.0.1:8077/;
    }
}
```

##### Main service:
```nginx
stream {
    upstream ssh {
        server 127.0.0.1:8077;
    }
    server {
        listen        7777;

        include /etc/nginx/whitelist.txt;
        deny all;
        
        proxy_pass    ssh;
    }
}
```