use std::sync::Arc;

use postgres_native_tls::MakeTlsConnector;
use r2d2::Pool;
use r2d2_postgres::PostgresConnectionManager;


pub struct AppState {
    pub destination: String,
    pub postgres_pool: Arc<Pool<PostgresConnectionManager<MakeTlsConnector>>>,
}
