extern crate dotenv;

use actix_web::web::{Bytes, self, Data};
use is_root::is_root;
use std::fs::OpenOptions;
use std::io::Write;
use std::sync::Arc;
use std::{thread, process, env};
use actix_web::middleware::Logger as MyLogger;
use dotenv::dotenv;
use native_tls::TlsConnector;
use postgres_native_tls::MakeTlsConnector;
use r2d2::Pool;
use r2d2_postgres::PostgresConnectionManager;
use std::fs;
use std::str;
use std::time::SystemTime;
use std::path::Path;
use chrono::{DateTime, Utc};
use current_platform::{COMPILED_ON, CURRENT_PLATFORM};
use actix_web::{get, Responder, HttpServer, App, HttpRequest, post};
mod app_state;
use crate::app_state::AppState;
use crate::client_request::ClientRequest;
mod client_request;

const PID_FILE: &str = "./pid.txt";

fn postgres_pool() -> Arc<Pool<PostgresConnectionManager<MakeTlsConnector>>> {
    let connector = TlsConnector::builder().build().unwrap();
    let connector: MakeTlsConnector = MakeTlsConnector::new(connector);
    let host = env::var("POSTGRES_HOST").expect("KXCCLV");
    let port = env::var("POSTGRES_PORT").expect("EHlmrw");
    let db_name = env::var("POSTGRES_DB_NAME").expect("LaWotj");
    let user = env::var("POSTGRES_USER").expect("gGiYUf");
    let password = env::var("POSTGRES_PASSWORD").expect("OQgsWZ");
    let sslmode = env::var("POSTGRES_SSLMODE").expect("hiVkGy");
    let psql = format!(
        "postgres://{}:{}@{}:{}/{}?sslmode={}",
        user, password, host, port, db_name, sslmode
    );
    let manager: PostgresConnectionManager<MakeTlsConnector> =
        PostgresConnectionManager::new(psql.parse().unwrap(), connector);
    // let manager: PostgresConnectionManager<MakeTlsConnector> = PostgresConnectionManager::new(
    //     "postgres://bom_user:bom_password@vochess.com/neondb"
    //         .parse()
    //         .unwrap(),
    //     connector,
    // );
    let pool = r2d2::Pool::builder().max_size(1).build(manager).unwrap();
    return Arc::new(pool);
}

pub fn migrate(pool: &Arc<Pool<PostgresConnectionManager<MakeTlsConnector>>>) {
    match pool.get() {
        Ok(mut conn) => {
            conn.simple_query(
                "
                CREATE TABLE IF NOT EXISTS TBL_ALLOW_LIST (
                    DEVICE_ID VARCHAR(40) NOT NULL,
                    PUBLIC_IP VARCHAR(15) NOT NULL,
                    DEVICE_INFO TEXT      NOT NULL,
                    UPDATE_TIME TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
                    PRIMARY KEY (DEVICE_ID)
                );
                ",
            )
            .expect("tXJFeK");
        }
        Err(_) => {
            panic!("Can't get connection from firebird pool");
        }
    }
}

pub fn upsert(pool: &Arc<Pool<PostgresConnectionManager<MakeTlsConnector>>>, device_id: String, public_ip: String, device_info: String) {
    match pool.get() {
        Ok(mut conn) => {
            conn.execute(
                "
                INSERT INTO TBL_ALLOW_LIST (DEVICE_ID, PUBLIC_IP, DEVICE_INFO)
                VALUES ($1, $2, $3)
                ON CONFLICT (DEVICE_ID) DO UPDATE SET
                    PUBLIC_IP = $2, 
                    DEVICE_INFO = $3, 
                    UPDATE_TIME = NOW();
                ",
                &[&device_id, &public_ip, &device_info],
            ).expect("5g5Ng9");
        }
        Err(_) => {
            panic!("Can't get connection from firebird pool");
        }
    }
}

pub fn find_all_ips(pool: &Arc<Pool<PostgresConnectionManager<MakeTlsConnector>>>) -> Vec<String> {
    match pool.get() {
        Ok(mut conn) => {
            let result_set = conn.query(
                "
                SELECT PUBLIC_IP FROM TBL_ALLOW_LIST ORDER BY DEVICE_ID;
                ",
                &[]
            ).expect("LdBFEN");
            let mut ips: Vec<String> = Vec::new();
            for row in result_set {
                let ip: String = row.get("PUBLIC_IP");
                ips.push(ip);
            }
            return ips;
        }
        Err(_) => {
            panic!("Can't get connection from firebird pool");
        }
    }
}

fn main() {
    // DOTENV:
    dotenv().ok();

    // CONFIG:
    let port = env::var("PORT").expect("65H4Ib").parse::<u16>().expect("SsPUdo");
    let destination = env::var("DESTINATION").expect("Fu4Pfe");

    // ROOT:
    if !is_root() {
        panic!("Is not root")
    }
    OpenOptions::new().append(false).write(true).read(true).truncate(true).create_new(!Path::new(&destination).exists()).open(&destination).expect("j5sbos");

    // PLATFORM:
    println!("CURRENT_PLATFORM = {}\nCOMPILED_ON = {}", CURRENT_PLATFORM, COMPILED_ON);

    // TIME:
    let now = time_now();

    // PID:
    let mut pid_file = OpenOptions::new().append(false).write(true).read(true).truncate(true).create_new(!Path::new(PID_FILE).exists()).open(PID_FILE).unwrap();
    let pid = process::id();
    pid_file.write_all(format!("Launch time = {}\nPID = {}\n", now, pid).as_ref()).unwrap();
    pid_file.flush().unwrap();

    // POSTGRES:
    let postgres_pool = postgres_pool();
    {
        let pool = postgres_pool.clone();
        let checking_postgres = thread::spawn(move || {
            let mut postgres_connection = pool.get().expect("HJZyNE");
            let result = postgres_connection
                .query(
                    "SELECT 'POSTGRES IS OK' AS CHECKING FROM pg_stat_activity;",
                    &[],
                )
                .expect("DcrhPy");
            let checking_postgres: String = result.get(0).unwrap().get("CHECKING");
            return checking_postgres;
        })
        .join()
        .expect("XaOKTy");
        println!("{}", checking_postgres);
    }
    {
        let pool = postgres_pool.clone();
        thread::spawn(move || {
            migrate(&pool);
        })
        .join()
        .expect("oRLyHQ");
        println!("{}", "MIGRATED");
    }

    // INIT:
    {
        let pool = postgres_pool.clone();
        let destination = destination.clone();
        thread::spawn(move || {
            update_allow_list(&pool, destination);
        }).join().expect("MG3PyX");
        println!("{}", "INITIALIZED");
    }
    
    // SERVER:
    let runtime = actix_rt::Runtime::new().unwrap();
    runtime.block_on(async move {
        run(postgres_pool, destination, port).await.unwrap();
    });
    println!("{}", "Server started!");
}

async fn run(postgres_pool: Arc<r2d2::Pool<PostgresConnectionManager<MakeTlsConnector>>>, destination: String, port: u16) -> std::io::Result<()> {
    return HttpServer::new(move || {
        return App::new()
            .app_data(web::Data::new(AppState {
                destination: destination.clone(),
                postgres_pool: postgres_pool.clone(),
            }))
            .service(serve)
            .wrap(MyLogger::default());
    })
    .workers(1)
    .bind(("0.0.0.0", port))
    .unwrap()
    .run()
    .await;
}

// curl -X POST "http://127.0.0.1:11435/http/AA6F18755F47B393C275046C7EBD37A8" -d '{"device_id": "DDD111", "public_ip": "192.168.0.1", "device_info": "My device Info"}' -v
#[post("/http/AA6F18755F47B393C275046C7EBD37A8")]
pub async fn serve(http_request: HttpRequest, bytes: Bytes) -> impl Responder {
    let http_body = String::from_utf8(bytes.to_vec()).unwrap();
    println!("{}", &http_body);
    let request = serde_json::from_str::<ClientRequest>(&http_body).unwrap();
    let app_state = http_request.app_data::<Data<AppState>>().unwrap();
    let pool = app_state.postgres_pool.clone();
    let destination = app_state.destination.clone();
    thread::spawn(move || {
        upsert(&pool, request.device_id, request.public_ip, request.device_info);
        println!("UPSERTED");
        update_allow_list(&pool, destination);
        println!("UPDATED");
    });
    // .expect("unqhFr");
    return "Accepted!";
}

pub fn update_allow_list(pool: &Arc<Pool<PostgresConnectionManager<MakeTlsConnector>>>, destination: String) {
    let current_ips: Vec<String> = fs::read_to_string(&destination).unwrap().trim().split("\n").map(|it| it.to_string()).map(|it| it.replace("allow ", "").replace(";", "")).collect();
    let current_ips_string = current_ips.join("\n").trim().to_string();
    let new_ips: Vec<String> = find_all_ips(pool);
    let new_ips_string = new_ips.iter().map(|it| format!("allow {};\n", it)).collect::<String>().trim().to_string();
    if !current_ips_string.eq(&new_ips_string) {
        println!("rFdXHq: {}", &new_ips_string);
        let mut destination_file = OpenOptions::new().append(false).write(true).read(true).truncate(true).create_new(!Path::new(&destination).exists()).open(&destination).expect("6UD76o");
        destination_file.write_all(new_ips_string.as_ref()).unwrap();
        destination_file.flush().unwrap();
        let (code, output, error) = run_script::run_script!("systemctl reload nginx").unwrap();
        println!("nCAq7e: {}, {}, {}", code, output, error);
    }
}

fn time_now() -> String {
    let now = SystemTime::now();
    let now: DateTime<Utc> = now.into();
    now.to_rfc3339()
}
