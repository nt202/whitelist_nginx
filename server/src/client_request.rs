use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct ClientRequest {
    pub device_id: String,
    pub public_ip: String,
    pub device_info: String,
}
