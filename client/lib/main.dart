import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences.getInstance().then((sp) async {
    final String? deviceId = await getDeviceId();
    if (deviceId == null) {
      runApp(const Splash(text: "No device id found"));
      return;
    }
    final String? deviceInfo = await getDeviceInfo();
    if (deviceInfo == null) {
      runApp(const Splash(text: "No device info found"));
      return;
    }
    runApp(MyApp(sp: sp, deviceId: deviceId, deviceInfo: deviceInfo));
  });
  runApp(const Splash(text: "Loading..."));
}

class Splash extends StatelessWidget {
  final String text;
  const Splash({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: Center(child: Text(text)),
    );
  }
}

class MyApp extends StatelessWidget {
  final SharedPreferences sp;
  final String deviceId;
  final String deviceInfo;

  const MyApp({super.key, required this.sp, required this.deviceId, required this.deviceInfo});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'whitelist_nginx',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: MyHomePage(title: 'whitelist_nginx', sp: sp, deviceId: deviceId, deviceInfo: deviceInfo),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;
  final SharedPreferences sp;
  final String deviceId;
  final String deviceInfo;

  const MyHomePage({super.key, required this.title, required this.sp, required this.deviceId, required this.deviceInfo});

  @override
  Widget build(BuildContext context) {
    final TextEditingController endpointController = TextEditingController();
    final String? endpoint = sp.getString("endpoint");
    if (endpoint != null && endpoint.isNotEmpty) {
      endpointController.text = endpoint;
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Spacer(),
            Container(
              width: 400.0,
              padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
              child: TextField(
                controller: endpointController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'https://mysite.com/service',
                ),
                onChanged: (value) {
                  sp.setString("endpoint", value.trim());
                },
              ),
            ),
            const Spacer(),
            const Text(
              'Device ID:',
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: FittedBox(
                fit: BoxFit.fitWidth,
                child: Text(
                  deviceId,
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ),
            ),
            const SizedBox(height: 20),
            const Text(
              'Device Info:',
            ),
            Text(
              deviceInfo,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(height: 20),
            const Text(
              'Public IP:',
            ),
            FutureBuilder(
                future: getIp(),
                initialData: "...",
                builder: (ctx, snap) {
                  return Text(
                    snap.data ?? "NO IP",
                    style: Theme.of(context).textTheme.headlineMedium,
                  );
                }),
            const Spacer(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final String? endpoint = sp.getString("endpoint");
          if (endpoint == null || endpoint.isEmpty) {
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text("Endpoint is empty"),
            ));
            return;
          }
          final String myIp = await getIp();
          final String body = jsonEncode({"device_id": deviceId, "public_ip": myIp, "device_info": deviceInfo});
          final http.Response response = await http.post(Uri.parse(endpoint), body: body).timeout(const Duration(seconds: 5));
          final String responseBody = "Code: ${response.statusCode}; Body: ${response.body}";
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(responseBody),
          ));
        },
        tooltip: 'Update',
        child: const Icon(Icons.refresh),
      ),
    );
  }

  Future<String> getIp() async {
    final http.Response response = await http.get(Uri.parse("https://ifconfig.me/all.json")).timeout(const Duration(seconds: 5));
    final String myIp = (jsonDecode(response.body) as Map<String, dynamic>)["ip_addr"];
    return myIp;
  }
}

Future<String?> getDeviceId() async {
  var deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) { // import 'dart:io'
    var iosDeviceInfo = await deviceInfo.iosInfo;
    return iosDeviceInfo.identifierForVendor; // unique ID on iOS
  } else if(Platform.isAndroid) {
    var androidDeviceInfo = await deviceInfo.androidInfo;
    return androidDeviceInfo.id; // unique ID on Android
  }
  return null;
}

Future<String?> getDeviceInfo() async {
  var deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) { // import 'dart:io'
    var iosDeviceInfo = await deviceInfo.iosInfo;
    return iosDeviceInfo.utsname.machine; // e.g. "iPod7,1"
  } else if(Platform.isAndroid) {
    var androidDeviceInfo = await deviceInfo.androidInfo;
    return androidDeviceInfo.model; // e.g. "Moto G (4)"
  }
  return null;
}